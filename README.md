# jmh-gradle-example

**Complete sample project that uses JMH with Gradle**

JMH is the [Java Microbenchmark Harness](https://github.com/openjdk/jmh#java-microbenchmark-harness-jmh).

This sample project has some differences/advantages over other sample projects on the Internet:

- very little configuration, mostly using annotations in the benchmark class
- no shadow JAR / UberJar required
- execute benchmarks directly from Gradle, but of course still in a dedicated JVM
- no plugin required, just Gradle


## Prerequisites

- Java 11 or newer


# Run it

In order to run the performance test, simply execute

    ./gradlew jmh

The full benchmark suite can take several hours to complete. That's normal, of course.


# Understand it

In order to understand better what is going on, you can:

- Read the comments in the *build.gradle* file
- Run `./gradlew tiTree jmh` - displays the task dependencies of our `jmh` task
- Run `./gradlew printCp` - prints the classpath that Gradle configures for the `jmh` task
- Run `./gradlew jmhHelp` - shows all the command line options of JMH


## Links

- [JMH Website](https://github.com/openjdk/jmh#java-microbenchmark-harness-jmh)
- Study the [JMH Samples](https://github.com/openjdk/jmh/tree/1.26/jmh-samples/src/main/java/org/openjdk/jmh/samples)


# License

This sample is provided under the terms of the [Apache License 2.0](LICENSE).


# Benchmark Results

This project only serves to show how to use JMH from Gradle. If you seriously want to find the best way to generate
a String of spaces in Java, you'd need more tests, and possibly longer iterations. Microbenchmarking in Java is a
complex topic. But, just in case anyone's interested, here's a chart of the results of this sample benchmark which I got
on my machine
([implementations](https://gitlab.com/barfuin/jmh-gradle-example/-/blob/master/src/main/java/org/barfuin/jmh/gradle/example/StringOfSpaces.java)):

[![Result Chart](README-1.thumb.png)](README-1.png)

Mind the logarithmic scale on the vertical axis! This means that a higher/better bar is actually *significantly* better.

I created the above chart manually. The result from the JMH run is just a JSON file with the data.
